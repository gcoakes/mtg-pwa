import preprocess from "svelte-preprocess";
import adapter from "@sveltejs/adapter-static";
import svg from "@poppanator/sveltekit-svg";

let paths = {};
if (process.env.CI_PAGES_URL) {
  const pagesUrl = new URL(process.env.CI_PAGES_URL);
  paths.base = pagesUrl.pathname;
}

/** @type {import('@sveltejs/kit').Config} */
const config = {
  // Consult https://github.com/sveltejs/svelte-preprocess
  // for more information about preprocessors
  preprocess: preprocess(),

  kit: {
    paths: paths,
    // hydrate the <div id="svelte"> element in src/app.html
    target: "#svelte",
    adapter: adapter({
      pages: "public",
    }),
    vite: {
      plugins: [svg({ type: "url" })],
    },
  },
};

export default config;
