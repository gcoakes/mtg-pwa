/// <reference lib="WebWorker" />
declare const self: ServiceWorkerGlobalScope;

import { registerRoute } from "workbox-routing";
import { CacheFirst, StaleWhileRevalidate } from "workbox-strategies";
import { ExpirationPlugin } from "workbox-expiration";
import { CacheableResponsePlugin } from "workbox-cacheable-response";
import { precacheAndRoute } from "workbox-precaching";
import { build, files, timestamp } from "$service-worker";

const STATIC_PRECACHE_ENTRIES = files.map((f) => ({
  url: f,
  revision: timestamp,
}));
const BUILD_PRECACHE_ENTRIES = build.map((b) => ({
  url: b,
  revision: null,
}));
const PRECACHE_ENTRIES = [
  { url: "./", revision: timestamp },
  ...BUILD_PRECACHE_ENTRIES,
  ...STATIC_PRECACHE_ENTRIES,
];

precacheAndRoute(PRECACHE_ENTRIES);

registerRoute(
  ({ request }) => request.destination === "document",
  new CacheFirst({
    cacheName: `documents-${timestamp}`,
    plugins: [new CacheableResponsePlugin({ statuses: [200] })],
  })
);

registerRoute(
  ({ request }) =>
    request.destination === "style" ||
    request.destination === "script" ||
    request.destination === "worker",
  new StaleWhileRevalidate({
    cacheName: `assets-${timestamp}`,
    plugins: [new CacheableResponsePlugin({ statuses: [200] })],
  })
);

registerRoute(
  ({ request }) => request.destination === "image",
  new CacheFirst({
    cacheName: "image",
    plugins: [
      new CacheableResponsePlugin({ statuses: [200] }),
      new ExpirationPlugin({
        maxAgeSeconds: 90 * 24 * 60 * 60, // 3 months per rotation.
        maxEntries: 1200, // About how many cards will be in standard at any point.
      }),
    ],
  })
);

registerRoute(
  ({ url }) => url.hostname == "api.scryfall.com",
  new StaleWhileRevalidate({
    cacheName: "api",
    plugins: [new CacheableResponsePlugin({ statuses: [200] })],
  })
);

self.addEventListener("install", () => {
  self.skipWaiting();
});
