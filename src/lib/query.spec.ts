import { render, fireEvent, RenderResult } from "@testing-library/svelte";
import Query from "$lib/Query.svelte";
import { queryFields } from "./stores";

describe("Query", () => {
  it("should be empty", async () => {
    const rendered: RenderResult = render(Query);
    expect(rendered.getByTestId("raw-query")).toHaveDisplayValue("");
  });
  it("should stay in sync with localStorage", async () => {
    const initial = {
      colorFilter: "Identity",
      colors: [],
      majorTypes: [],
      manaCostType: "CMC",
      manaCost: "1",
      format: "standard",
      extra: "",
    };
    localStorage.setItem("query", JSON.stringify(initial));
    const rendered: RenderResult = render(Query);
    const rawQuery = rendered.getByTestId("raw-query");
    expect(rawQuery).toHaveDisplayValue("cmc=1 f:standard");
    await fireEvent.click(rendered.getByTestId("color-R"));
    expect(rawQuery).toHaveDisplayValue("id:r cmc=1 f:standard");
    initial.colors.push("R");
    expect(localStorage.getItem("query")).toBe(JSON.stringify(initial));
  });
  afterEach(() => {
    queryFields.reset();
  });
});
