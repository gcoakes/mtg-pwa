import type { Subscriber, Unsubscriber, Updater, Writable } from "svelte/store";
import { writable } from "svelte/store";
import { ColorFilter, ManaCostType } from "$lib/api";
import type { Card, Format, MajorType } from "$lib/api";

export class Persistable<T> implements Writable<T> {
  private unsubscriber: Unsubscriber | null;
  private listener: (e: StorageEvent) => void;
  public set: (value: T) => void;
  public update: (updater: Updater<T>) => void;
  public subscribe: (
    run: Subscriber<T>,
    invalidate?: (value?: T) => void
  ) => Unsubscriber;

  constructor(initial?: T) {
    const inner = writable(initial);
    this.unsubscriber = null;
    this.listener = null;
    this.set = inner.set;
    this.update = inner.update;
    this.subscribe = inner.subscribe;
  }

  /**
   * Override the store by the contents of localStorage, then persist changes to
   * that same key.
   *
   * @returns The old value of the store, if overridden.
   */
  public persist(key: string, clearValue: T): T | null {
    if (this.unsubscriber) {
      this.unsubscriber();
    }
    if (this.listener) {
      window.removeEventListener("storage", this.listener);
    }
    const stored = localStorage.getItem(key);
    let old = null;
    if (stored) {
      this.update((x) => {
        old = x;
        return JSON.parse(stored);
      });
    }
    this.unsubscriber = this.subscribe((x) =>
      localStorage.setItem(key, JSON.stringify(x))
    );
    window.addEventListener(
      "storage",
      (this.listener = (e) => {
        if (e.key === key) {
          const stored = localStorage.getItem(key);
          if (stored) {
            this.set(JSON.parse(stored));
          } else {
            this.set(clearValue);
          }
        }
      })
    );
    return old;
  }
}

export declare type QueryFields = {
  colorFilter: ColorFilter;
  colors: string[];
  majorTypes: MajorType[];
  manaCostType: ManaCostType;
  manaCost: string;
  format: Format | null;
  extra: string;
};

export class QueryFieldsStore extends Persistable<QueryFields> {
  constructor() {
    super(QueryFieldsStore.default());
  }
  reset(): void {
    this.set(QueryFieldsStore.default());
  }
  static default(): QueryFields {
    return {
      colorFilter: ColorFilter.Identity,
      colors: [],
      majorTypes: [],
      manaCostType: ManaCostType.Converted,
      manaCost: "",
      format: null,
      extra: "",
    };
  }
}

export const queryFields = new QueryFieldsStore();

export declare type DeckEntry = {
  card: Card;
  count: number;
};

export class DeckStore extends Persistable<Record<string, DeckEntry>> {
  add(card: Card): void {
    this.update((d) => {
      if (card.id in d) {
        d[card.id].count++;
      } else {
        d[card.id] = { card: card, count: 1 };
      }
      return d;
    });
  }
  remove(card: Card): void {
    this.update((d) => {
      if (card.id in d) {
        if (d[card.id].count > 1) {
          d[card.id].count--;
        } else {
          delete d[card.id];
        }
      }
      return d;
    });
  }
}

export const deck = new DeckStore({});
