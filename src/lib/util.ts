export function debounce(fn: (...args) => void, time: number): () => void {
  let tid;
  return function (...args) {
    clearTimeout(tid);
    tid = setTimeout(() => {
      fn.apply(this, args);
    }, time);
  };
}
