const API = "https://api.scryfall.com";

export declare type CardSearchQuery = {
  q?: string;
  unique?: "cards" | "art" | "prints";
  order?:
    | "name"
    | "set"
    | "released"
    | "rarity"
    | "color"
    | "usd"
    | "tix"
    | "eur"
    | "cmc"
    | "power"
    | "toughness"
    | "edhrec"
    | "artist";
  dir?: "auto" | "asc" | "desc";
  include_extras?: boolean;
  include_multilingual?: boolean;
  include_variations?: boolean;
  page?: number;
  format?: "json" | "csv";
  pretty?: boolean;
};
export declare type CardImageUris = {
  png?: string;
  large?: string;
  normal?: string;
  small?: string;
};
export declare type Card = {
  id: string;
  name: string;
  image_uris?: CardImageUris;
  card_faces?: CardFace[];
  prices: {
    usd?: string;
    usd_foil?: string;
    usd_etched?: string;
    eur?: string;
    tix?: string;
  };
};
export declare type CardFace = {
  image_uris: CardImageUris;
};
export declare type Page = {
  total_cards: number;
  has_more: boolean;
  data: Card[];
};

export async function cardsSearch(query: CardSearchQuery): Promise<Page> {
  const resp = await fetch(
    `${API}/cards/search?${new URLSearchParams(
      query as Record<string, string>
    )}`
  );
  return await resp.json();
}

export enum Colors {
  White = "W",
  Blue = "U",
  Black = "B",
  Red = "R",
  Green = "G",
}
export const COLORS: string[] = Object.values(Colors);

export function bestImage(card: Card | CardFace): string {
  if (card.image_uris) {
    return (
      card.image_uris.png ||
      card.image_uris.large ||
      card.image_uris.normal ||
      card.image_uris.small
    );
  } else if ("card_faces" in card) {
    const face = card.card_faces[0];
    return (
      face.image_uris.png ||
      face.image_uris.large ||
      face.image_uris.normal ||
      face.image_uris.small
    );
  }
}

export enum ColorFilter {
  Identity = "Identity",
  AtLeast = "At Least",
  AtMost = "At Most",
  Exactly = "Exactly",
}

export enum MajorType {
  Creature = "creature",
  Planeswalker = "planeswalker",
  Instant = "instant",
  Sorcery = "sorcery",
  Enchantment = "enchantment",
  Artifact = "artifact",
  Land = "land",
  Legendary = "legendary",
}

export enum ManaCostType {
  Converted = "CMC",
  AtLeast = "At Least",
  AtMost = "At Most",
  Exactly = "Exactly",
}

export enum Format {
  Standard = "standard",
  Future = "future",
  Historic = "historic",
  Gladiator = "gladiator",
  Pioneer = "pioneer",
  Modern = "modern",
  Legacy = "legacy",
  Pauper = "pauper",
  Vintage = "vintage",
  Penny = "penny",
  Commander = "commander",
  Brawl = "brawl",
  HistoricBrawl = "historicbrawl",
  PauperCommander = "paupercommander",
  Duel = "duel",
  OldSchool = "oldschool",
  PreModern = "premodern",
}

export function colorQuery(filter: ColorFilter, colors: string[]): string {
  const c = colors.map((c) => c.toLowerCase()).join("");
  switch (filter) {
    case ColorFilter.Identity:
      return `id:${c}`;
    case ColorFilter.AtLeast:
      return `c>=${c}`;
    case ColorFilter.AtMost:
      return `c<=${c}`;
    case ColorFilter.Exactly:
      return `c:${c}`;
  }
}

export function majorTypeQueries(types: MajorType[]): string[] {
  return types.map((t) => `t:${t}`);
}

export function manaCostQuery(type: ManaCostType, cost: string): string {
  const c = cost.replace(/\s+/g, "");
  switch (type) {
    case ManaCostType.Converted:
      return `cmc=${c}`;
    case ManaCostType.AtLeast:
      return `m>=${c}`;
    case ManaCostType.AtMost:
      return `m<=${c}`;
    case ManaCostType.Exactly:
      return `m=${c}`;
  }
}

export function formatQuery(format: Format): string {
  return `f:${format}`;
}
